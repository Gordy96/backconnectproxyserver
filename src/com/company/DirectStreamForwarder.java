package com.company;

import com.company.exceptions.AuthorizationException;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class DirectStreamForwarder extends StreamForwarder {
    final private static String auth_header = "Proxy-Authorization";
    private boolean authorized = false;
    private Authenticator authenticator;
    public DirectStreamForwarder(InputStream i, OutputStream o) {
        super(i, o);
    }
    public DirectStreamForwarder(InputStream i, OutputStream o, Authenticator auth) {
        this(i, o);
        authenticator = auth;
    }

    @Override
    protected void interrupt(byte[] buffer, int read) throws AuthorizationException {
        if(!authorized){
            String[] temp = new String(buffer, 0, read).split("\r\n\r\n")[0].split("\r\n");
            String[] t = temp[0].split(" ");
            String method = t[0];
            String path = t[1];
            String protocol = t[2];
            temp[0] = "";
            HashMap<String, String> headers = new HashMap<>();
            for(String s : temp){
                String[] parts = s.split(":");
                if(parts.length > 1)
                    headers.put(parts[0], parts[1].trim());
            }

            if(!authenticator.check(headers.get(auth_header)))
                throw new AuthorizationException(headers.containsKey(auth_header));
            else
                authorized = true;
        }
    }
}
