package com.company;

public class Level {
    private String name;
    private int code;

    public Level(String name, int code) {
        this.code = code;
        this.name = name;
    }

    public String toString() {
        return this.name;
    }

    public static Level ERROR = new Level("ERROR", 1000);
    public static Level WARNING = new Level("WARNING", 800);
    public static Level INFO = new Level("INFO", 600);
    public static Level DEBUG = new Level("DEBUG", 200);
}
