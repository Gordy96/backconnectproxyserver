package com.company;

import java.io.IOException;
import java.net.*;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.function.BiConsumer;

import org.eclipse.jetty.server.Server;

import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.webapp.WebAppContext;

public class Main {

    public static void main(String[] args) throws Exception {

        byte[] addr = new byte[]{(byte)0,(byte)0, 0, 0};
        final InetAddress address = InetAddress.getByAddress(addr);

        HashMap<String, String> arguments = new HashMap<>();

        arguments.put("service_port", "9091");
        arguments.put("control_port", "9090");
        arguments.put("api_uri", "0.0.0.0:80");
        arguments.put("dump_stderr", "false");
        for(int i = 0; i < args.length; i++){
            if((args[i].equals("-s") || args[i].equals("--service")) && i + 1 < args.length && !args[i + 1].startsWith("-")){
                arguments.put("service_port", args[i+1]);
                i++;
            } else if((args[i].equals("-p") || args[i].equals("--port")) && i + 1 < args.length && !args[i + 1].startsWith("-")){
                arguments.put("control_port", args[i+1]);
                i++;
            } else if((args[i].equals("-a") || args[i].equals("--api")) && i + 1 < args.length && !args[i + 1].startsWith("-")){
                String arg = args[i+1];
                if(arg.contains(".")){
                    if(!arg.contains(":"))
                        arg += ":80";
                } else if(arg.matches("[0-9]{2,5}")) {
                        arg = "0.0.0.0:" + arg;
                }
                arguments.put("api_uri", arg);
                i++;
            } else if (args[i].equals("-d") || args[i].equals("--dump-stderr")) {
                arguments.put("dump_stderr", "true");
            }
        }

        Container.getInstance().setAdditionalSocketPort(Integer.valueOf(arguments.get("service_port")));

        arguments.forEach(new BiConsumer<String, String>() {
            @Override
            public void accept(String s, String s2) {
                Logger.instance(System.out).Info("%s: %s", s, s2);
            }
        });

        Server server;
        server = new Server(parseIp(arguments.get("api_uri")));

        HandlerCollection handlers = new HandlerCollection();
        WebAppContext webapp1 = new WebAppContext();
        String webDir = Main.class.getResource("/webapp").toExternalForm();
        webapp1.setResourceBase(webDir);
        webapp1.setContextPath("/");
        handlers.addHandler(webapp1);

        server.setHandler(handlers);

        if(arguments.getOrDefault("dump_stderr", "false").equals("true"))
            server.dumpStdErr();
        try{
            server.start();
        } catch (Exception e){
            Logger.instance(System.out).Error(e);
            System.exit(0);
        }

        Container container = Container.getInstance();
        DB.getInstance();

        ServerSocket s = new ServerSocket(Integer.valueOf(arguments.get("control_port")), 10, address);
        Socket client;

        Thread wSocketThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    ServerSocket listener = new ServerSocket(container.getAdditionalSocketPort(), 10, address);
                    listener.setReuseAddress(true);
                    Socket connect;
                    while((connect = listener.accept()) != null){
                        String key = connect.getInetAddress().toString().substring(1);
                        Logger.instance(System.out).Info("new socket for %s; handler running: %b", key, container.getList().containsKey(key));
                        if(container.getList().containsKey(key)){
                            container.getList().get(key).pushSocket(connect);
                        } else {
                            connect.close();
                        }
                    }
                } catch(Exception e){
                    Logger.instance(System.out).Error(e);
                }
            }
        });

        wSocketThread.start();

        final ExecutorService pool = Executors.newCachedThreadPool();

        while((client = s.accept()) != null){
            String key = client.getInetAddress().toString().substring(1);
            Logger.instance(System.out).Info("connection from %s", key);
            try{
                if(container.getList().containsKey(key)){
                    container.getList().get(key).kill();
                    Logger.instance(System.out).Error("trying connect with ip: %s. Killing previous instance", key);
                }
                Handler thread = new Handler(client, pool);
                container.getList().put(key, thread);
                container.getListByTag().put(thread.getPeer().getTag(), thread);
                thread.start();
            } catch (IOException e){
                Logger.instance(System.out).Error(e);
            }
        }
    }
    private static InetSocketAddress parseIp(String ip) throws UnknownHostException {
        String[] parts = ip.split(":");
        byte[] addr = new byte[4];
        int port = 0;
        if(parts.length > 1 && parts[1].length() > 0)
            port = Integer.valueOf(parts[1]);
        parts = parts[0].split("\\.");
        for(int i = 0; i < parts.length; i++){
            int ippart = (int)Integer.valueOf(parts[i]);
            if(ippart > 255)
                throw new UnknownHostException();
            addr[i] = (byte)ippart;
        }
        InetSocketAddress address;
        address = new InetSocketAddress(InetAddress.getByAddress(addr), port);
        return address;
    }
}
