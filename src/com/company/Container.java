package com.company;

import java.util.HashMap;

public class Container {
    private static Container instance = null;

    public static Container getInstance(){
        if(instance == null)
            instance = new Container();
        return instance;
    }

    private HashMap<String, Handler> list;
    private HashMap<String, Handler> list_by_tag;
    private Authenticator auth;

    private Container(){
        list = new HashMap<>();
        list_by_tag = new HashMap<>();
        auth = Authenticator.getInstance();
    }

    public HashMap<String, Handler> getList() {
        return list;
    }
    public HashMap<String, Handler> getListByTag() {
        return list;
    }

    public Authenticator getAuth(){
        return auth;
    }

    private int add_soc_port = 9091;
    public void setAdditionalSocketPort(int p){
        add_soc_port = p;
    }
    public int getAdditionalSocketPort(){
        return add_soc_port;
    }
}
