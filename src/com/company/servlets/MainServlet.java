package com.company.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import com.company.ConnectionHandler;
import com.company.Container;
import com.company.Handler;
import com.company.Peer;
import com.company.exceptions.WrongTagException;
import org.json.*;

import com.company.Pair;

@SuppressWarnings("serial")
public class MainServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String[] parts = request.getRequestURI().substring(1).split("/");
        switch(parts.length){
            case 2:
                handleList(request, response);
                break;
            case 3:
                handleListFor(request, response, parts[2]);
                break;
            case 4:
                handleAction(request, response, parts[2], parts[3]);
                break;
            default:
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    private void sendJsonResponse(HttpServletResponse response, JSONObject o) throws IOException {
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);
        response.getWriter().println(o.toString());
    }

    private void sendJsonResponse(HttpServletResponse response, JSONObject o, int status) throws IOException {
        response.setContentType("application/json");
        response.setStatus(status);
        response.getWriter().println(o.toString());
    }

    private void handleList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Container container = Container.getInstance();
        JSONObject res = new JSONObject();
        JSONObject proxies = new JSONObject();
        String action = request.getParameter("action");
        if (action != null && !action.equals("")){
            ArrayList<Peer> all = Peer.all();
            if ("add_credentials".equals(action)){
                JSONTokener tokener = new JSONTokener(request.getParameter("source"));
                JSONArray root = new JSONArray(tokener);
                if(root.isEmpty()){
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
                ArrayList<Pair<String,String>> newList = new ArrayList<>();
                for(int i = 0; i < root.length(); i++){
                    JSONObject temp = root.getJSONObject(i);
                    if(temp.has("username") && temp.has("password")) {
                        Pair<String, String> pair = new Pair<>(temp.getString("username"), temp.getString("password"));
                        newList.add(pair);
                    }
                }
                for (Peer peer : all)
                    peer.addCredentials(newList);
            } else if ("delete_credentials".equals(action)){
                JSONTokener tokener = new JSONTokener(request.getParameter("source"));
                JSONArray root = new JSONArray(tokener);
                if(root.isEmpty()){
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
                ArrayList<String> newList = new ArrayList<>();
                for(int i = 0; i < root.length(); i++){
                    String username = root.getString(i);
                    newList.add(username);
                }

                for (Peer peer : all)
                    peer.deleteCredentials(newList);
            } else if ("add_whitelist".equals(action)){
                JSONTokener tokener = new JSONTokener(request.getParameter("source"));
                JSONArray root = new JSONArray(tokener);
                if(root.isEmpty()){
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
                ArrayList<String> newList = new ArrayList<>();
                for(int i = 0; i < root.length(); i++){
                    String ip = root.getString(i);
                    if(ip.matches("\\b(?:(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\.){3}(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\b"))
                        newList.add(ip);
                }

                for (Peer peer : all)
                    peer.addToWhitelist(newList);
            } else if ("delete_whitelist".equals(action)){
                JSONTokener tokener = new JSONTokener(request.getParameter("source"));
                JSONArray root = new JSONArray(tokener);
                if(root.isEmpty()){
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
                ArrayList<String> newList = new ArrayList<>();
                for(int i = 0; i < root.length(); i++){
                    String ip = root.getString(i);
                    if(ip.matches("\\b(?:(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\.){3}(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\b"))
                        newList.add(ip);
                }

                for (Peer peer : all)
                    peer.deleteFromWhitelist(newList);
            }
        }

        if ("active".equals(request.getParameter("show"))){
            for(String key : container.getList().keySet()){
                Handler h = container.getList().get(key);
                HashMap<String, Object> obj = new HashMap<>();
                String address = h.getPeer().getIp().toString().substring(1);
                obj.put("address", address);
                obj.put("port", h.getPeer().getPort());
                obj.put("connections", h.activeConnections());
                proxies.put(h.getPeer().getTag(), obj);
            }
        } else {
            ArrayList<Peer> list = Peer.all();
            for(Peer peer : list){
                HashMap<String, Object> obj = new HashMap<>();
                obj.put("address", peer.getIp().toString().substring(1));
                obj.put("port", peer.getPort());
                proxies.put(peer.getTag(), obj);
            }
        }
        res.put("status", true);
        res.put("proxies", proxies);
        sendJsonResponse(response, res);
    }

    private void handleListFor(HttpServletRequest request, HttpServletResponse response, String tag) throws ServletException, IOException {
        Container container = Container.getInstance();
        JSONObject res = new JSONObject();
        JSONArray list = new JSONArray();
        Peer peer;
        Handler h = null;
        if(container.getListByTag().containsKey(tag)){
            h = container.getListByTag().get(tag);
            peer = h.getPeer();
        }
        else {
            try{
                peer = Peer.find(tag);
            } catch (WrongTagException e){
                sendJsonResponse(response, res, 404);
                return;
            }
        }
        JSONObject obj = new JSONObject();
        obj.put("port", peer.getPort());
        obj.put("address", peer.getIp().toString().substring(1));
        if(h != null)
            obj.put("connections", h.activeConnections());

        if ("add_credentials".equals(request.getParameter("action"))){
            JSONTokener tokener = new JSONTokener(request.getParameter("source"));
            JSONArray root = new JSONArray(tokener);
            if(root.isEmpty()){
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
            ArrayList<Pair<String,String>> newList = new ArrayList<>();
            for(int i = 0; i < root.length(); i++){
                JSONObject temp = root.getJSONObject(i);
                if(temp.has("username") && temp.has("password")) {
                    Pair<String, String> pair = new Pair<>(temp.getString("username"), temp.getString("password"));
                    newList.add(pair);
                }
            }
            peer.addCredentials(newList);
        } else if ("delete_credentials".equals(request.getParameter("action"))){
            JSONTokener tokener = new JSONTokener(request.getParameter("source"));
            JSONArray root = new JSONArray(tokener);
            if(root.isEmpty()){
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
            ArrayList<String> newList = new ArrayList<>();
            for(int i = 0; i < root.length(); i++){
                String username = root.getString(i);
                newList.add(username);
            }
            peer.deleteCredentials(newList);
        } else if ("add_whitelist".equals(request.getParameter("action"))){
            JSONTokener tokener = new JSONTokener(request.getParameter("source"));
            JSONArray root = new JSONArray(tokener);
            if(root.isEmpty()){
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
            ArrayList<String> newList = new ArrayList<>();
            for(int i = 0; i < root.length(); i++){
                String ip = root.getString(i);
                if(ip.matches("\\b(?:(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\.){3}(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\b"))
                    newList.add(ip);
            }
            peer.addToWhitelist(newList);
        } else if ("delete_whitelist".equals(request.getParameter("action"))){
            JSONTokener tokener = new JSONTokener(request.getParameter("source"));
            JSONArray root = new JSONArray(tokener);
            if(root.isEmpty()){
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
            ArrayList<String> newList = new ArrayList<>();
            for(int i = 0; i < root.length(); i++){
                String ip = root.getString(i);
                if(ip.matches("\\b(?:(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\.){3}(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\b"))
                    newList.add(ip);
            }
            peer.deleteFromWhitelist(newList);
        }

        ArrayList<String> credentials = peer.getCredentials();
        obj.put("credentials", credentials);

        ArrayList<String> whitelist = peer.getWhitelist();
        obj.put("whitelist", whitelist);

        res.put("status", true);
        res.put("proxy", obj);
        sendJsonResponse(response, res);
    }

    private void handleAction(HttpServletRequest request, HttpServletResponse response, String key, String action) throws ServletException, IOException {
        Container container = Container.getInstance();
        Handler handler;
        if((handler = container.getList().get(key)) != null){
            switch (action){
                case "toggle":
                    handler.commandAirplaneModeToggle();
                    //handler.kill();
                    JSONObject res = new JSONObject();
                    res.put("status", true);
                    sendJsonResponse(response, res);
                    break;
                default:
                    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        }
    }
}
