package com.company.exceptions;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class AuthorizationException extends Exception {
    private boolean header_present = false;
    public AuthorizationException(boolean present) {
        super();
        header_present = present;
    }

    public String render(){
        return "HTTP/1.1 407 Proxy Authentication Required\r\n" +
                "Date: " + new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z").format(new Date()) + "\r\n" +
                "Proxy-Authenticate: Basic realm=\"Access to internal proxy server\"\r\n\r\n";
    }
}
