package com.company;

import org.sqlite.SQLiteException;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

public class DB {
    private static DB instance = null;

    public static DB getInstance(){
        try{
            if(instance == null)
                instance = new DB();
        } catch(Exception e){
            return null;
        }
        return instance;
    }

    private Connection connection;

    private DB() throws ClassNotFoundException, SQLException, IOException {
        File file;
        if(!(file = new File("db.s3db")).exists())
            file.createNewFile();
        Class.forName("org.sqlite.JDBC");
        connection = DriverManager.getConnection("jdbc:sqlite:db.s3db");
        migrate();
    }

    public int delete(String query, ArrayList<Object> params){
        try{
            PreparedStatement statement =   connection.prepareStatement(query);
            int i = 1;
            for(Object o : params){
                statement.setObject(i++, o);
            }
            return statement.executeUpdate();
        } catch (Exception e){
            return -1;
        }
    }

    public ResultSet select(String query, ArrayList<Object> params){
        try{
            PreparedStatement statement =   connection.prepareStatement(query);
            int i = 1;
            for(Object o : params){
                statement.setObject(i++, o);
            }
            return statement.executeQuery();
        } catch (Exception e){
            return null;
        }
    }

    public int update(String query, ArrayList<Object> params){
        try{
            PreparedStatement statement =   connection.prepareStatement(query);
            int i = 1;
            for(Object o : params){
                statement.setObject(i++, o);
            }
            return statement.executeUpdate();
        } catch (Exception e){
            return -1;
        }
    }

    public int insert(String query, ArrayList<Object> params){
        try{
            PreparedStatement statement =   connection.prepareStatement(query);
            int i = 1;
            for(Object o : params){
                statement.setObject(i++, o);
            }
            return statement.executeUpdate();
        } catch (Exception e){
            return -1;
        }
    }

    private void migrate(){
        try{
            Statement statement = connection.createStatement();
            statement.execute("CREATE TABLE if not exists 'peers' (" +
                    "'id' INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "'tag' TEXT, " +
                    "'ip' INTEGER, " +
                    "'port' INTEGER)");
            statement.execute("CREATE TABLE if not exists 'credentials' (" +
                    "'id' INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "'username' TEXT, " +
                    "'password' TEXT, " +
                    "'peer_id' INTEGER," +
                    "FOREIGN KEY(peer_id) REFERENCES peers(id)," +
                    "UNIQUE(username, peer_id) ON CONFLICT REPLACE)");
            statement.execute("CREATE TABLE if not exists 'whitelist' (" +
                    "'id' INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "'ip' TEXT, " +
                    "'peer_id' INTEGER," +
                    "FOREIGN KEY(peer_id) REFERENCES peers(id)," +
                    "UNIQUE(ip, peer_id) ON CONFLICT REPLACE)");
        } catch (Exception e){
        }
    }
}
