package com.company;

import com.company.exceptions.AuthorizationException;
import com.company.exceptions.WrongTagException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.Stack;
import java.util.concurrent.*;
import java.util.function.Consumer;

public class Handler extends Thread {
    private Socket sock;
    private Peer peer;
    private ServerSocket listener;
    private Stack<Socket> sockets;
    private int connections = 0;

    public Peer getPeer(){
        return peer;
    }

    private int getRandomPort(){
        ArrayList<Peer> list = Peer.all();
        boolean cont = list.size() > 0;
        int port = 0;
        while(cont){
            port = ThreadLocalRandom.current().nextInt(2000, 65535);
            for(int i = 0; i < list.size(); i++){
                cont = port == list.get(i).getPort();
            }
        }
        return port;
    }

    private ExecutorService pool;

    public Handler(Socket p, ExecutorService pool) throws IOException {
        super();
        this.pool = pool;
        sock = p;
        byte[] buffer = new byte[256];
        int read = sock.getInputStream().read(buffer, 0, buffer.length);
        String tag = new String(buffer, 0, read);
        try{
            peer = Peer.find(tag);
        } catch (WrongTagException wte){
            peer = new Peer(sock.getInetAddress(), 0, tag);
        }
        Container container = Container.getInstance();
        Handler handler = container.getListByTag().get(tag);

        if (handler != null){
            handler.kill();
            container.getListByTag().remove(tag);
        }

        Logger.instance(System.out).Info("%s(%s) connected", tag, peer.getIp().toString());

        sockets = new Stack<>();
    }

    private boolean accepting = true;
    private boolean stopping = false;

    @Override
    public void run() {

        int port = peer.getPort() == 0 ? getRandomPort() : peer.getPort();
        int tries = 0;
        while(true){
            try{
                listener = new ServerSocket(port, 10, InetAddress.getByAddress(new byte[]{0,0,0,0}));
                listener.setReuseAddress(true);
                break;
            } catch (Exception e){
                Logger.instance(System.out).Error(
                        "%s(%s) got exception \n%s\n trying to bind port %d on try #%d",
                        peer.getTag(),
                        peer.getIp().toString(),
                        Logger.formatException(e),
                        peer.getPort(),
                        tries
                );
                if (tries > 100){
                    port = getRandomPort();
                    tries = 0;
                } else {
                    tries++;
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException ex) {
                        Logger.instance(System.out).Error(ex);
                    }
                }
            }
        }
        port = listener.getLocalPort();
        if(peer.getPort() != port || sock.getInetAddress() != peer.getIp()){
            peer.setPort(port);
            peer.setIp(sock.getInetAddress());
            peer.save();
        }

        long current_time = System.currentTimeMillis();
        long no_new_socket_timeout = 60000;
        try{
            Socket connect;
            while((connect = listener.accept()) != null && !sock.isClosed()){
                if(!accepting){
                    sock.getOutputStream().flush();
                    throw new Exception();
                }
                if(sock.isClosed()){
                    connect.close();
                    return;
                }

                ArrayList<String> whitelist = peer.getWhitelist();
                if (whitelist.size() > 0 && !Authenticator.checkIp(connect.getInetAddress(), whitelist)){
                    try{
                        connect.getOutputStream().write(new AuthorizationException(false).render().getBytes());
                    } catch (IOException e){

                    } finally {
                        try {
                            closeSocket(connect);
                        } catch (Throwable e) {
                            //Logger.instance(System.out).Error("could not close outgoing socket: %s", Logger.formatException(e));
                        }
                    }
                    continue;
                }

                if(sockets.size() <= 0){
                    this.requestAdditionalSocket();
                    current_time = System.currentTimeMillis();
                }
                while(sockets.size() <= 0){
                    if(sock.isClosed() || ((System.currentTimeMillis() - current_time) > no_new_socket_timeout)){
                        Logger.instance(System.out).Error("%s: cannot retrieve socket", peer.getTag());
                        connect.close();
                        return;
                    }
                }
                ConnectionHandler thread = new ConnectionHandler(connect, sockets.pop(), true, 60000);
                thread.setPeer(peer);
                thread.setPool(pool);
                pool.submit(thread);
                connections++;
                thread.onClose(new BeforeCloseHandler() {
                    @Override
                    public void Handle(ConnectionHandler ch) {
                        Logger.instance(System.out).Info("%s: transfer done (%s)", peer.getTag(), ch.toString());
                        connections--;
                    }

                    @Override
                    public void Exception(ConnectionHandler ch, Throwable exception) {
                        Logger.instance(System.out).Error(exception);
                    }
                });
            }
        } catch (Exception e){
            Logger.instance(System.out).Error("%s: got exception in handler", peer.getTag());
            Logger.instance(System.out).Error(e);
        } finally {
            synchronized (this){
                stopping = true;
            }
            Logger.instance(System.out).Warning("%s removing from list", peer.getTag());
            Container.getInstance().getListByTag().remove(peer.getTag());
            Container.getInstance().getList().remove(peer.getIp().toString().substring(1));
            try {
                closeSocket(sock);
            } catch (Throwable e) {
                Logger.instance(System.out).Error(e);
            }
            try {
                listener.close();
            } catch (Throwable e) {
                Logger.instance(System.out).Error(e);
            }
            synchronized (this){
                stopping = false;
            }
        }
    }

    private static void closeSocket(Socket s) throws IOException {
        try {
            s.getOutputStream().flush();
        } finally {
            try {
                s.getOutputStream().close();
            } finally {
                try {
                    s.getInputStream().close();
                } finally {
                    s.close();
                }
            }
        }
    }

    private void sendCommand(String command) throws IOException {
        char[] cmd = command.toCharArray();
        byte[] bytes = new byte[cmd.length];
        for(int i = 0; i < cmd.length; i++)
            bytes[i] = (byte)cmd[i];
        sock.getOutputStream().write(bytes);
        sock.getOutputStream().flush();
    }

    public void requestAdditionalSocket() throws IOException {
        sendCommand("create_socket|"+String.valueOf(Container.getInstance().getAdditionalSocketPort()));
    }
    public void commandAirplaneModeToggle() throws IOException {
        sendCommand("restart_network");
        accepting = false;
    }

    public void pushSocket(Socket socket){
        sockets.push(socket);
    }
    public int activeConnections(){
        return connections;
    }

    public void kill(){
        synchronized (this){
            stopping = true;
        }
        try{
            sock.close();
            listener.close();
            long start = System.currentTimeMillis();
            while(stopping) {
                Thread.sleep(10);
                if (System.currentTimeMillis() - start > 60000) {
                    throw new Exception("could not close listener thread for " + peer.getTag());
                }
            }
        } catch (Exception e){
            Logger.instance(System.out).Error(e);
        }
    }
}
