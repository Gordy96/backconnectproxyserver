package com.company;

import com.company.exceptions.AuthorizationException;

import javax.naming.AuthenticationException;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

public class ConnectionHandler implements Runnable {
    private Socket from;
    private Socket to;
    private boolean st = false;
    private int t_msec = 15000;
    private BeforeCloseHandler handler;
    private ExecutorService pool;

    public ConnectionHandler(Socket connection, Socket proxy){
        super();
        from = connection;
        to = proxy;
    }

    public ConnectionHandler(Socket connection, Socket proxy, boolean should_timeout){
        this(connection, proxy);
        st = should_timeout;
    }

    public ConnectionHandler(Socket connection, Socket proxy, boolean should_timeout, int timeout){
        this(connection, proxy, should_timeout);
        t_msec = timeout;
    }

    public void setPool(ExecutorService pool) {
        this.pool = pool;
    }

    private Peer peer;

    public void setPeer(Peer p){
        peer = p;
    }

    public void onClose(BeforeCloseHandler h){
        handler = h;
    }

    private boolean authorized = false;

    private Authenticator auth;
    final private static String auth_header = "Proxy-Authorization";

    @Override
    public void run() {
        auth =  new Authenticator();
        auth.setSource(peer.getCredentials().toArray(new String[]{}));
        try (final InputStream clientInput = new BufferedInputStream(from.getInputStream());
             final OutputStream clientOutput = from.getOutputStream()) {

            from.setKeepAlive(false);
            from.setTcpNoDelay(true);
            from.setReuseAddress(true);
            if (st)
                from.setSoTimeout(t_msec);
            to.setKeepAlive(false);
            to.setTcpNoDelay(true);
            to.setReuseAddress(true);
            if (st)
                to.setSoTimeout(t_msec);

            HttpProxyClientHeader header = HttpProxyClientHeader.parseFrom(clientInput);
            if (auth.requiresAuthentication()) {
                if(!auth.check(header.getHeaders().get(auth_header))){
                    AuthorizationException e = new AuthorizationException(header.getHeaders().containsKey(auth_header));
                    String exc = e.render();
                    try{
                        from.getOutputStream().write(exc.getBytes());
                        from.getOutputStream().flush();
                    } catch (IOException ignored) { }
                    return;
                }
            }

            final OutputStream remoteOutput = to.getOutputStream();
            final InputStream remoteInput = to.getInputStream();

            header.getHeaders().remove(auth_header);
            remoteOutput.write(header.getBytes());

            Future<?> future = pool.submit(new Callable<Object>() {
                @Override
                public Object call() {
                    pipe(clientInput, remoteOutput);
                    return null;
                }
            });
            pipe(remoteInput, clientOutput);
            future.get();
        } catch (Throwable e) {
            handler.Exception(this, e);
        } finally {
            try {
                closeSocket(to);
            } catch (IOException e) {}
            try {
                closeSocket(from);
            } catch (IOException e) {}
            handler.Handle(this);
        }
    }

    public static void closeSocket(Socket s) throws IOException {
        try {
            s.getOutputStream().flush();
        } finally {
            try {
                s.getOutputStream().close();
            } finally {
                try {
                    s.getInputStream().close();
                } finally {
                    s.close();
                }
            }
        }
    }

    private void pipe(InputStream in, OutputStream out) {
        byte[] buf = new byte[4096];
        int len;
        try {
            while ((len = in.read(buf)) != -1) {
                out.write(buf, 0, len);
            }
            out.flush();
        } catch (IOException ignored) {}
    }
}
