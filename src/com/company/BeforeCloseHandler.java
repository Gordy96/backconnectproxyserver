package com.company;

public abstract class BeforeCloseHandler {
    public abstract void Handle(ConnectionHandler ch);
    public abstract void Exception(ConnectionHandler ch, Throwable exception);
}
