package com.company;

import com.company.exceptions.AuthorizationException;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

public class StreamForwarder extends Thread {
    protected static final int MAX_SKIP_BUFFER_SIZE = 2048;
    protected static final int DEFAULT_BUFFER_SIZE = 8192;
    protected InputStream is;
    protected OutputStream os;
    protected boolean running = true;

    protected StreamForwarder pair;

    public StreamForwarder(InputStream i, OutputStream o) {
        is = i;
        os = o;
    }

    public void setPair(StreamForwarder f) {
        pair = f;
    }

    public void kill(){
        try{
            is.close();
            os.close();
        } catch (Throwable e){
            Logger.instance(System.out).Error(e);
        }
        running = false;
    }

    @Override
    public void run() {
        try{
            int read;
            byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
            while(running && (read = is.read(buffer, 0, DEFAULT_BUFFER_SIZE)) > 0) {
                interrupt(buffer, read);
                os.write(buffer, 0, read);
            }
            os.flush();
        } catch (AuthorizationException authException){
            String exc = authException.render();
            try {
                Logger.instance(System.out).Info("sending 407");
                pair.tell(exc.getBytes());
            } catch(Exception e){
                Logger.instance(System.out).Error("error sending 407 %s", e);
            }
        } catch (SocketException | SocketTimeoutException e) {
            //TODO: I removed SocketException logging cuz class always generates it
        } catch (Throwable e){
            Logger.instance(System.out).Error(e);
        }
    }

    public void tell(byte[] bytes){
        try{
            os.write(bytes);
            os.flush();
        } catch (IOException e){
            Logger.instance(System.out).Error(e);
        }
    }

    protected void interrupt(byte[] buffer, int read) throws AuthorizationException {
    }
}
