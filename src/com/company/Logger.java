package com.company;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;
import java.util.HashMap;

public class Logger {
    private static HashMap<OutputStream, Logger> instances = new HashMap<>();


    private OutputStream os;

    private String format = "[{{time}}] {{level}}: {{format}}\n";

    private Logger(OutputStream os) {
        this.os = os;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    private void log(Level lvl, String message, Object ...args) {
        String fmt = format;
        if (fmt.contains("{{level}}")) {
            fmt = fmt.replace("{{level}}", lvl.toString());
        }
        if (fmt.contains("{{time}}")) {
            Date now = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
            fmt = fmt.replace("{{time}}", sdf.format(now));
        }
        fmt = fmt.replace("{{format}}", message);
        try {
            this.os.write(String.format(fmt, args).getBytes());
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void Error(Throwable ex) {
        this.Error("exception occurred %s", formatException(ex));
    }

    public void Error(String message, Object ...args) {
        this.log(Level.ERROR, message, args);
    }

    public void Warning(String message, Object ...args) {
        this.log(Level.WARNING, message, args);
    }

    public void Info(String message, Object ...args) {
        this.log(Level.INFO, message, args);
    }

    public static Logger instance(OutputStream os){
        return instances.getOrDefault(os, new Logger(os));
    }

    public static String formatException(Throwable e){
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        String sStackTrace = sw.toString();
        pw.close();
        try {
            sw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return sStackTrace;
    }
}
