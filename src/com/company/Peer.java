package com.company;

import com.company.exceptions.WrongTagException;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Peer {
    private int id = 0;
    private InetAddress ip;

    public InetAddress getIp() {
        return ip;
    }

    public void setIp(InetAddress ip) {
        this.ip = ip;
    }

    public void setIp(String ip) throws UnknownHostException {
        ByteBuffer temp = ByteBuffer.allocate(4);
        for(String part : ip.split("\\."))
            temp.put(Byte.valueOf(part));
        this.ip = InetAddress.getByAddress(temp.array());
    }

    private int port;

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    private String tag;

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }

    public static Peer find(String tag) throws WrongTagException {
        ArrayList<Object> params = new ArrayList<>();
        params.add(tag);
        ResultSet result = DB.getInstance().select("SELECT * FROM \"peers\" WHERE \"tag\" = ?", params);
        if(result == null)
            throw new WrongTagException();
        Peer peer = new Peer();
        try{
            result.next();
            peer.ip = InetAddress.getByAddress(ByteBuffer.allocate(4).putInt(result.getInt("ip")).array());
            peer.port = result.getInt("port");
            peer.tag = result.getString("tag");
            peer.id = result.getInt("id");
        } catch (Exception e){
            throw new WrongTagException();
        }
        return peer;
    }

    public static ArrayList<Peer> all(){
        ArrayList<Peer> res = new ArrayList<>();
        ResultSet result = DB.getInstance().select("SELECT * FROM \"peers\"", new ArrayList<>());
        try{
            while(result.next()){
                Peer peer = new Peer();
                peer.ip = InetAddress.getByAddress(ByteBuffer.allocate(4).putInt(result.getInt("ip")).array());
                peer.port = result.getInt("port");
                peer.tag = result.getString("tag");
                peer.id = result.getInt("id");
                res.add(peer);
            }
        } catch (Exception e){
        }

        return res;
    }

    private Peer(){}

    public Peer(InetAddress address, int port, String tag){
        ip = address;
        this.port = port;
        this.tag = tag;
    }

    public boolean save(){
        ArrayList<Object> params = new ArrayList<>();
        params.add(tag);
        params.add(ByteBuffer.wrap(ip.getAddress()).getInt());
        params.add(port);
        boolean result;
        if(id > 0){
            params.add(id);
            result = DB.getInstance().update("INSERT INTO \"peers\" (\"tag\", \"ip\", \"port\", \"id\") VALUES (?,?,?,?)", params) == 1;
        } else {
            result = DB.getInstance().insert("INSERT INTO \"peers\" (\"tag\", \"ip\", \"port\") values (?,?,?)", params) == 1;
        }
        return result;
    }

    public ArrayList<String> getCredentials(){
        ArrayList<Object> params = new ArrayList<>();
        params.add(id);
        ResultSet result = DB.getInstance().select("SELECT * FROM \"credentials\" WHERE \"peer_id\" = ?", params);
        ArrayList<String> credentials = new ArrayList<>();
        try{
            while(result.next()){
                credentials.add(result.getString("username") + ":" + result.getString("password"));
            }
        } catch (Exception e){
        }
        return credentials;
    }

    public int addCredentials(ArrayList<Pair<String,String>> list){
        ArrayList<Object> params = new ArrayList<>(3);
        params.add(0);
        params.add(0);
        params.add(id);
        int added = 0;
        for(Pair<String,String> entry : list){
            params.set(0, entry.getKey());
            params.set(1, entry.getValue());
            added += DB.getInstance().insert("INSERT OR REPLACE INTO \"credentials\" (\"username\",\"password\",\"peer_id\") VALUES (?,?,?)", params);
        }
        return added;
    }

    public int deleteCredentials(ArrayList<String> list){
        ArrayList<Object> params = new ArrayList<>();
        params.add(0, "");
        params.add(1,id);
        int added = 0;
        for(String username : list){
            params.set(0, username);
            added += DB.getInstance().delete("DELETE FROM \"credentials\" WHERE \"username\" = ? AND \"peer_id\" = ?", params);
        }
        return added;
    }

    public ArrayList<String> getWhitelist(){
        ArrayList<Object> params = new ArrayList<>();
        params.add(id);
        ResultSet result = DB.getInstance().select("SELECT * FROM \"whitelist\" WHERE \"peer_id\" = ?", params);
        ArrayList<String> credentials = new ArrayList<>();
        try{
            while(result.next()){
                credentials.add(result.getString("ip"));
            }
        } catch (Exception e){
        }
        return credentials;
    }

    public int addToWhitelist(ArrayList<String> list){
        ArrayList<Object> params = new ArrayList<>(2);
        params.add(0);
        params.add(id);
        int added = 0;
        for(String entry : list){
            params.set(0, entry);
            added += DB.getInstance().insert("INSERT OR REPLACE INTO \"whitelist\" (\"ip\",\"peer_id\") VALUES (?,?)", params);
        }
        return added;
    }

    public int deleteFromWhitelist(ArrayList<String> list){
        ArrayList<Object> params = new ArrayList<>();
        params.add(0, "");
        params.add(1,id);
        int added = 0;
        for(String username : list){
            params.set(0, username);
            added += DB.getInstance().delete("DELETE FROM \"whitelist\" WHERE \"ip\" = ? AND \"peer_id\" = ?", params);
        }
        return added;
    }
}
