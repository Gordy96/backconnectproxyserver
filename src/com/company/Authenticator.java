package com.company;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Base64;

public class Authenticator {
    private static Authenticator instance = null;

    public static Authenticator getInstance(){
        if(instance == null)
            instance = new Authenticator();
        return instance;
    }

    public boolean requiresAuthentication() {
        return entries.length > 0;
    }

    private String[] entries;
    private String[] ips;
    public boolean check(String credentials){
        if(entries.length == 0) {
            return true;
        }
        if(credentials != null && credentials.contains("Basic")){
            String[] parts = credentials.split(" ");
            if(parts.length > 1)
                credentials = credentials.split(" ")[1].trim();
            else
                return false;
            for(String entry : entries)
                if(Base64.getEncoder().encodeToString(entry.getBytes()).equals(credentials))
                    return true;
        }
        return false;
    }

    public static boolean checkIp(InetAddress ip, ArrayList<String> whitelist){
        for(String i : whitelist)
            if(ip.toString().substring(1).equals(i))
                return true;
        return false;
    }

    public boolean checkIp(InetAddress ip){
        for(String i : ips)
            if(ip.toString().substring(1).equals(i))
                return true;
        return false;
    }

    public void setSource(String[] source) {
        entries = source;
    }

    public void setIps(String[] whitelist) {
        ips = whitelist;
    }
}
